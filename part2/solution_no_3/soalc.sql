SELECT result.SchoolYear, sum(result.TotalPrice) AS TotalRevenue from 

(SELECT transaction.UniqueTransactionId, transaction.UniqueUserId,
UserData.Email, Profile.SchoolYear, Profile.Name, packages.PackageName, packages.TotalPrice, transaction.CompletedOrderTransactionDate  
FROM pahamify.transaction AS transaction

LEFT JOIN pahamify.packages as packages
ON transaction.UniquePackageId = packages.PackageId

LEFT JOIN pahamify.user_profile as Profile
ON transaction.UniqueUserId = Profile.UniqueId

LEFT JOIN pahamify.user_data as UserData
ON transaction.UniqueUserId = UserData.UniqueId

GROUP BY transaction.UniqueUserId, packages.PackageName
) As result

group by result.SchoolYear