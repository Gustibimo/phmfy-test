SELECT  Profile.UniqueId, Profile.Name, UserData.Email, UserData.Phone, Profile.Gender,Profile.SchoolYear,
Profile.District, Status.SubscriptionEndDate, Status.CurrentDiamond
FROM pahamify.user_profile as Profile

LEFT JOIN pahamify.user_data as UserData
ON Profile.UniqueId = UserData.UniqueId

LEFT JOIN pahamify.user_status as Status ON Profile.UniqueId = Status.UniqueId
WHERE SubscriptionEndDate > now();