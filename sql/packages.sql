-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 11, 2020 at 08:05 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `PackageId` varchar(36) COLLATE utf8mb4_bin NOT NULL,
  `PackageName` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` mediumtext COLLATE utf8mb4_bin DEFAULT NULL,
  `PackageBenefit` mediumtext COLLATE utf8mb4_bin DEFAULT NULL,
  `TotalPrice` int(11) DEFAULT NULL,
  `StickerPrice` int(11) DEFAULT NULL,
  `PromoDescription` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `PackageCategory` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`PackageId`, `PackageName`, `description`, `PackageBenefit`, `TotalPrice`, `StickerPrice`, `PromoDescription`, `PackageCategory`) VALUES
('021ca281-b694-43e8-936a-9cbf98cbbdce', 'Paket Reward 5-Diamond for Einstein Student', 'Hadiah 5 Diamond untuk kamu yang sudah membeli paket Einstein periode Juli-Oktober', 'Mendapatkan 5 Diamond gratis', 0, 0, '', 'DIAMOND'),
('05d8d076-d2da-460a-aa43-7b27287caeef', 'Paket 5 Diamond ', 'Mendapatkan 5 buah diamond yang bisa kamu gunakan untuk ikutan Tryout UTBK Pahamify di Test Center', 'Mendapatkan 5 buah diamond yang bisa kamu gunakan untuk ikutan Tryout UTBK Pahamify di Test Center', 25000, 0, '', 'DIAMOND'),
('05e6fb42-f783-461c-b19b-66e9598d13ab', 'Paket Marie Curie Tokopedia (3 Bulan)', 'Mendapatkan akses seluruh materi Pahamify selama 3 bulan (90 hari).\n🎁Dapatkan bonus avatar topi Tokopedia untuk pembelian voucher Pahamify via Tokopedia', 'Dapatkan akses ke seluruh materi belajar Pahamify selama 90 hari. Perhitungan paket aktif akan dihitung mulai 1 Agustus 2019, jadi kamu tetap bisa mengakses seluruh materi Pahamify secara gratis hinggal 31 Juli 2019.\n\n🎁 Bonus Avatar 🎁\n- Topi Tokopedia', 85000, 370000, 'Special Launch Discount 70%', 'SUBSCRIPTION'),
('1532b93e-5153-4a88-ac4b-ef6eac679607', 'Paket Tesla (6 Bulan)', 'Mendapatkan akses seluruh materi Pahamify selama 6 bulan (180 hari). \n🎁Bonus avatar untuk pembelian  sebelum 1 Agustus 2019!', 'Dapatkan akses ke seluruh materi belajar Pahamify selama 180 hari. Perhitungan paket aktif akan dihitung mulai 1 Agustus 2019, jadi kamu tetap bisa mengakses seluruh materi Pahamify secara gratis hinggal 31 Juli 2019.\n\n🎁 Bonus Avatar 🎁\n- Backgound Pahamify\n- Baju Pahamify\n- Topi Pahamify\n- Gadget Pahamify', 170000, 570000, 'Special Launch Discount 70%', 'SUBSCRIPTION'),
('1cb3dfd6-eb8a-42ca-8f72-56fc95f78f82', 'Paket 5 Diamond', 'Mendapatkan 5 buah Diamond yang bisa kamu gunakan untuk ikutan Tryout UTBK Pahamify', 'Mendapatkan 5 buah Diamond yang bisa kamu gunakan untuk ikutan Tryout UTBK Pahamify', 25000, 0, '', 'DIAMOND'),
('37695149-ebaf-4c88-8766-5470860be535', 'Paket Bundling Tryout Saintek/Soshum', 'Dapatkan 45 Diamond yang bisa kamu gunakan untuk 10 kali Try Out untuk persiapan UTBK 2020 dan 1 kali Konsultasi dengan Psikolog Pahamify', 'Mendapatkan 45 Diamond yang bisa kamu gunakan untuk 10 kali Try Out untuk persiapan UTBK 2020 dan 1 kali Konsultasi dengan Psikolog Pahamify', 100000, 150000, 'Promo Harbolnas', 'DIAMOND'),
('42842ed8-65ee-4f01-9e5c-fe351cd75a76', 'Paket Bundling Tryout Campuran', 'Dapatkan 65 Diamond yang bisa kamu gunakan untuk 10 kali Try Out paket campuran (IPC) untuk persiapan UTBK 2020 dan 1 kali Konsultasi dengan Psikolog Pahamify', 'Mendapatkan 65 Diamond yang bisa kamu gunakan untuk 10 kali Try Out paket campuran (IPC) untuk persiapan UTBK 2020 dan 1 kali Konsultasi dengan Psikolog Pahamify', 175000, 250000, 'Promo Harbolnas', 'DIAMOND'),
('43827957-7c98-436d-9a9b-2eb47096ef67', 'Paket Tesla Tokopedia (6 Bulan)', 'Mendapatkan akses seluruh materi Pahamify selama 6 bulan (180 hari). \n🎁Dapatkan bonus avatar topi Tokopedia untuk pembelian voucher Pahamify via Tokopedia', 'Dapatkan akses ke seluruh materi belajar Pahamify selama 180 hari. Perhitungan paket aktif akan dihitung mulai 1 Agustus 2019, jadi kamu tetap bisa mengakses seluruh materi Pahamify secara gratis hinggal 31 Juli 2019.\n\n🎁 Bonus Avatar 🎁\n- Topi Tokopedia', 130000, 570000, 'Special Launch Discount 70%', 'SUBSCRIPTION'),
('65f46971-e2c1-4ae9-b52e-4b88de5d5e8e', 'Paket 5 Diamond TEST EMAIL', 'Mendapatkan 5 buah Diamond yang bisa kamu gunakan untuk ikutan Tryout UTBK Pahamify', 'Mendapatkan 5 buah Diamond yang bisa kamu gunakan untuk ikutan Tryout UTBK Pahamify', 25000, 0, '', 'DIAMOND'),
('7742bfa4-099d-46d5-9328-4b679a677c3b', 'Paket Marie Curie (3 Bulan)', 'Mendapatkan akses seluruh materi Pahamify selama 3 bulan (90 hari).\n🎁Bonus avatar untuk pembelian  sebelum 1 Agustus 2019!', 'Dapatkan akses ke seluruh materi belajar Pahamify selama 90 hari. Perhitungan paket aktif akan dihitung mulai 1 Agustus 2019, jadi kamu tetap bisa mengakses seluruh materi Pahamify secara gratis hinggal 31 Juli 2019.\n\n🎁 Bonus Avatar 🎁\n- Backgound Pahamify\n- Baju Pahamify\n- Topi Pahamify\n- Gadget Pahamify', 110000, 370000, 'Special Launch Discount 70%', 'SUBSCRIPTION'),
('9c85d23a-47f0-4e51-bb96-2597e95615aa', 'Registrasi Try Out UTBK#3 IPC', 'Registrasi Try Out UTBK#3 IPC', 'Registrasi Try Out UTBK#3 IPC', 15000, 0, '', 'DIAMOND'),
('9e7135ca-753b-4e5b-bff1-c27d45d1fe85', 'Paket Einstein + Diamond + Buku Hutata (1 Tahun)', 'Mendapatkan akses seluruh materi Pahamify selama 1 tahun (365 hari), 5 Diamond dan Buku Belajar Cara Belajar versi Cetak', 'Dapatkan akses ke seluruh materi belajar Pahamify selama 365 hari \n\n🎁 Bonus  🎁\n- Buku Belajar Cara Belajar versi cetak*\n- Gratis 5 Diamond\n- Avatar Topi Pahamify\n\n*CS akan menghubungi dalam 1 x 24 jam, untuk mendata alamat pengiriman\n*Gratis ongkir Jabodetabek\n*Info lebih lanjut: bit.ly/bukuHutata', 450000, 1020000, 'Diskon 55%', 'SUBSCRIPTION'),
('a604df3b-e114-462c-b5ef-e96464b2ab60', 'Paket Einstein (1 Tahun)', 'Mendapatkan akses seluruh materi Pahamify selama 1 tahun (365 hari). \n🎁Bonus avatar untuk pembelian  sebelum 1 Agustus 2019!', 'Dapatkan akses ke seluruh materi belajar Pahamify selama 365 hari. Perhitungan paket aktif akan dihitung mulai 1 Agustus 2019, jadi kamu tetap bisa mengakses seluruh materi Pahamify secara gratis hinggal 31 Juli 2019.\n\n🎁 Bonus Avatar 🎁\n- Backgound Pahamify\n- Baju Pahamify\n- Topi Pahamify\n- Gadget Pahamify', 260000, 870000, 'Special Launch Discount 70%', 'SUBSCRIPTION'),
('c78c0b14-06b7-4896-8920-b7089e178c3f', 'Tryout gratis UTBK 2020 #1 - Soshum', 'Dapatkan akses tryout UTBK Soshum gratis dengan follow social media Pahamify dan mention 5 teman kamu', 'Dapatkan akses tryout UTBK Soshum gratis dengan follow social media Pahamify dan mention 5 teman kamu', 0, 0, '', 'DIAMOND'),
('d0718646-93df-4a51-82bd-aea676dfaf18', 'Registrasi Try out UTBK #3 - Saintek', 'Pendaftaran Try Out UTBK#3 untuk peminatan Saintek', 'Pendaftaran Try Out UTBK#3 untuk peminatan Saintek', 14997, 0, '', 'DIAMOND'),
('d2961f68-9350-4b2b-939a-474bad8dc928', 'Paket Newton Tokopedia (1 Bulan)', 'Mendapatkan akses seluruh materi Pahamify selama 1 bulan (30 hari). \n🎁Dapatkan bonus avatar topi Tokopedia untuk pembelian voucher Pahamify via Tokopedia', 'Dapatkan akses ke seluruh materi belajar Pahamify selama 30 hari. Perhitungan paket aktif akan dihitung mulai 1 Agustus 2019, jadi kamu tetap bisa mengakses seluruh materi Pahamify secara gratis hinggal 31 Juli 2019.\n\n🎁 Bonus Avatar 🎁\n- Topi Tokopedia', 40000, 170000, 'Special Launch Discount 70%', 'SUBSCRIPTION'),
('d4020aeb-ff7b-49bd-871c-c1a206bd8a3f', 'Paket Einstein Tokopedia (1 Tahun)', 'Mendapatkan akses seluruh materi Pahamify selama 1 tahun (365 hari). \n🎁Dapatkan bonus avatar topi Tokopedia untuk pembelian voucher Pahamify via Tokopedia', 'Dapatkan akses ke seluruh materi belajar Pahamify selama 365 hari. Perhitungan paket aktif akan dihitung mulai 1 Agustus 2019, jadi kamu tetap bisa mengakses seluruh materi Pahamify secara gratis hinggal 31 Juli 2019.\n\n🎁 Bonus Avatar 🎁\n- Topi Tokopedia', 200000, 870000, 'Special Launch Discount 70%', 'SUBSCRIPTION'),
('f623443a-d9cf-4302-97da-d29fd2696997', 'Paket Newton (1 Bulan)', 'Mendapatkan akses seluruh materi Pahamify selama 1 bulan (30 hari). \n🎁Bonus avatar untuk pembelian  sebelum 1 Agustus 2019!', 'Dapatkan akses ke seluruh materi belajar Pahamify selama 30 hari. Perhitungan paket aktif akan dihitung mulai 1 Agustus 2019, jadi kamu tetap bisa mengakses seluruh materi Pahamify secara gratis hinggal 31 Juli 2019.\n\n🎁 Bonus Avatar 🎁\n- Backgound Pahamify\n- Baju Pahamify\n- Topi Pahamify\n- Gadget Pahamify', 50000, 170000, 'Special Launch Discount 70%', 'SUBSCRIPTION'),
('ff300ac9-89ef-4483-a7e5-cff93d05c1fa', 'Tryout gratis UTBK 2020 #1 - Saintek', 'Dapatkan akses tryout UTBK Saintek gratis dengan follow social media Pahamify dan mention 5 teman kamu', 'Dapatkan akses tryout UTBK Saintek gratis dengan follow social media Pahamify dan mention 5 teman kamu', 0, 0, '', 'DIAMOND');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`PackageId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
